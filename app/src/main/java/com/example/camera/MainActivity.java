package com.example.camera;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //imageView = findViewById(R.id.imageView);
        cameraLouncher.launch(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
    }

    ActivityResultLauncher<Intent> cameraLouncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == RESULT_OK){
                        Bundle extras = result.getData().getExtras();
                        Bitmap imgBitmap = (Bitmap) extras.get("data");

                        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                        Toast.makeText(getApplicationContext(), "Colocando imagen...", Toast.LENGTH_SHORT).show();
                        try {
                            wallpaperManager.getInstance(getApplicationContext()).setBitmap(imgBitmap);
                            finish();
                        } catch (IOException e) {
                            Toast.makeText(getApplicationContext(), "Error al colocar la imgen de fondo", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                    else
                        finish();
                }
            });
}